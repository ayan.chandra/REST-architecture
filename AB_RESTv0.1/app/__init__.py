from flask import *
from flask_api import FlaskAPI
from flask_mongoalchemy import *
from Database.querywrapper import QueryWrapper
from Database.querywrapper import*

import json

# break this file into several endpointService class
# local import
from instance.config import app_config

# initialize py-mongo-client

db = MongoAlchemy()

def create_app(config_name):
    app = FlaskAPI(__name__, instance_relative_config=True)
    app.config.from_object(app_config[config_name])
    app.config.from_pyfile('config.py')
    #needs to be updated for mongodb
    app.config['MONGOALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app)

    #add the routing
    @app.route('/', methods=['GET','POST'])
    def sendHomeResponse():
        res= {}
        return res

    @app.route('/getToken', methods=['GET', 'POST'])
    def serveGetToken():
        res = {}
        return res

    @app.route('/suspendToken', methods=['GET', 'POST'])
    def serveSuspendToken():
        res = {}
        return res
    '''
    @app.route('/auth', methods=['GET', 'POST'])
    def serveAuth():
        if request.method == 'POST':
            if request.headers.get('param') == 3 :
                #write something
            
        elif request.method == 'GET':
                #shit happens
        else:
            #shit happens
        res = {}
        return res
    '''
    @app.route('/update', methods=['GET', 'POST'])
    def serveUpdate():
        res = {}
        return res

    @app.route('/notification', methods=['GET', 'POST'])
    def serveNotification():
        res = {}
        return res

    @app.route('/batterylog', methods=['GET', 'POST'])
    def serveBatterylog():
        res = {}

        return res

    @app.route('/batteryhealthlog', methods=['GET', 'POST'])
    def serveBatteryHealthlog():
        res = {}
        return res

    @app.route('/activity', methods=['GET', 'POST'])
    def serveVehicleActivity():
        res={}
        error = None
        if request.method == 'POST':
            if request.headers['content-type'] == 'application/json':
                # ensure valid json
                try:
                    data_object = request.json
                except ValueError:
                    res = jsonify({'error': '405'})
            #print(data_object['vehicle_id'])
            if checkVehicleId(data_object['vehicle_id']):
                insert_vehicle_activity(data_object)
                res = jsonify({'status': 'success'})
            else:
                res = jsonify({'error': 'invalid data'})
        #elif request.method == 'GET':
        else:
            if request.headers['content-type'] == 'application/json':
                # ensure valid json
                #data_object = json.load(request.data)
                val = request.headers['vehicle_id']
                val2= request.headers['app_id']
                print(val)
                #res = jsonify({'error': '405'})
                if(val!=0):
                    if checkVehicleId(val):
                        get_v_act = getVehicleActivity(val)
                        res = jsonify({'vehicle_activity_time': get_v_act.vehicle_activity_time,
                                    'vehicle_activity_status': get_v_act.vehicle_activity_status})
                elif(val2 !=0):
                    v_id = getVehicleId(val2)
                    get_v_act = getVehicleActivity(v_id.vehicle_id)
                    res = jsonify({'vehicle_activity_time': get_v_act.vehicle_activity_time,
                                'vehicle_activity_status': get_v_act.vehicle_activity_status})
        return res

    @app.route('/status', methods=['GET', 'POST'])
    def getStatus():
        res = {}
        return res

    @app.route('/routing', methods=['GET', 'POST'])
    def serveRouting():
        res = {}
        return res

    @app.route('/locate', methods=['GET', 'POST'])
    def serveLocate():
        res = {}
        return res

    return app