from Database.querywrapper import QueryWrapper
from app import db
#modelRef = db.Document

""" 
    Vehicle Modules and their models are in this 
    module. Models related to vehicle as a overall
    unit, app, user are kept in this segment. Please 
    add, delete or update models related to vehicle
    in this part.

"""


class VehicleManagement(db.Document):
    """   write model for vehicle management  """

    query_class = QueryWrapper
    vehicle_id = db.StringField()
    vehicle_model = db.StringField()
    time_on_road = db.IntField()
    cost = db.FloatField()
    issued_By = db.StringField()
    release_date_on_road = db.StringField()
    warranty_period = db.IntField()
    is_verified = db.IntField()
    headlight_service_Date = db.StringField()
    tail_light_service_date = db.StringField()
    air_Conditioning_service_Date = db.StringField()
    motor_service_Date = db.StringField()
    battery_service_Date = db.StringField()


class UserDetails(db.Document):
    """ write model for  user details  """

    query_class = QueryWrapper
    user_name = db.StringField()
    password = db.StringField()
    user_contact_number = db.IntField()
    user_mail_id = db.StringField()
    user_registration_id = db.StringField()
    user_license_id = db.StringField()
    User_adhaar_id = db.IntField()
    user_id = db.StringField()
    vehicle_list = db.DocumentField()


class AppDetails(db.Document):
    """ write model for  App details  """

    query_class = QueryWrapper
    app_id = db.StringField()
    app_ver = db.StringField()
    current_allowed_min_ver = db.StringField()


class VehicleUserMapping(db.Document):
    """ write model for vehicle user mapping  """

    query_class = QueryWrapper
    vehicle_id = db.StringField()
    user_id = db.StringField()
    current_rest_ver = db.StringField()
    current_OTA_ver = db.StringField()
    purchase_Date = db.StringField()
    puchase_type = db.StringField()
    termination_Date = db.StringField()


class VehicleAppMapping(db.Document):
    """ write model for vehicle app mapping  """

    query_class = QueryWrapper
    vehicle_id = db.StringField()
    app_id = db.StringField()
    positive_contactor_state = db.StringField()
    platform = db.StringField()
    auth_verified_on = db.StringField()

class VehicleSpecs(db.Document):
    """ write model for  Vehicle spec details  """

    query_class = QueryWrapper
    vehicle_id = db.StringField()
    vehicle_type = db.StringField()
    engine_type = db.StringField()
    engine_model = db.StringField()
    power_consumption = db.FloatField()
    motor_type = db.StringField()
    motor_model = db.StringField()
    acceleration = db.FloatField()
    braking_power = db.FloatField()
    brake_model = db.StringField()
    battery_type = db.StringField()
    battery_model = db.StringField()


class VehicleTravelDetails(db.Document):
    """ write model for  Vehicle travel details  """

    query_class = QueryWrapper
    vehicle_id = db.StringField()
    distance_covered = db.FloatField()
    power_consumed = db.FloatField()
    regenerative_braking_power = db.FloatField()
    timestamp = db.StringField()


class VehicleActivity(db.Document):
    """   write model for vehicle activity track update  """

    query_class = QueryWrapper
    vehicle_activity_time = db.StringField()
    vehicle_activity_status =  db.IntField() # only allowed values are 0 and 1, 0--> OFF, 1-->ON
    vehicle_id = db.StringField()


class VehicleIssuerManagement(db.Document):
    """ write model for  Vehicle issuer management  """

    query_class = QueryWrapper
    issuer_id = db.StringField()
    issuer_name = db.StringField()
    issuer_mail_id = db.StringField()
    issuer_employee_code = db.IntField()


""" 
    Battery Modules and their models are in this 
    module. Models related to battery as a overall
    unit are kept in this segment. Please add
    , delete or update models related to battery
    in this part.

"""


class BatteryState(db.Document):
    """ write model for  battery state  """

    query_class = QueryWrapper


class BatteryHealth(db.Document):
    """ write model for  battery health  """

    query_class = QueryWrapper


class BatteryChildCellsState(db.Document,BatteryState):
    """ write model for  battery child cell state details  """

    query_class = QueryWrapper


class BatteryChildCellsHealth(db.Document, BatteryHealth):
    """ write model for  battery child cell health details  """

    query_class = QueryWrapper


class InfoFetchTimeMapping(db.Document):
    """ write model for  Information fetch time mapping details  """

    query_class = QueryWrapper
    interval = db.IntField()
    table_name = db.StringField()


""" 
    Non-Battery Modules and their models are in this 
    module. Models related to battery as a overall
    unit are kept in this segment. Please add
    , delete or update models related to non-battery
    in this part.

"""


class MandatoryNonBatteryCriticalPartState(db.Document):
    """ write model for  mandatory non-battery critical part state  """

    query_class = QueryWrapper
    timestamp = db.StringField()
    vehicle_id = db.StringField()
    negative_contactor_state = db.StringField()
    positive_contactor_state = db.StringField()
    pre_charge_relay_state = db.StringField()


class AuxiliaryStatus(db.Document):
    """ write model for  mandatory non-battery auxiliary part state  """

    query_class = QueryWrapper
    headlight_status = db.StringField()
    tail_light_status = db.StringField()
    air_conditioning_Status = db.StringField()
    timestamp = db.StringField()
    vehicle_id = db.StringField()


""" 
    Access management Modules and their models are in this 
    module.Please add
    , delete or update models related to Access management
    in this part.

"""


class AccessManagement(db.Document):
    """ write model for  Access management   """

    query_class = QueryWrapper
    dev_type = db.StringField()
    table_name = db.StringField()
    is_read_granted = db.IntField()
    is_write_granted = db.IntField()

""" 
    Location or routing Modules and their models are in this 
    module.Please add
    , delete or update models related to location
    in this part.

"""


class LocationVehicleMapping(db.Document):
    """ write model for  location vehicle mapping details  """

    query_class = QueryWrapper
    vehicle_id = db.StringField()
    current_Longitude = db.FloatField()
    current_Latitude = db.FloatField()
    timestamp = db.StringField()


""" 
    Problem or error Modules and their models are in this 
    module.Please add
    , delete or update models related to disaster
    in this part.

"""


class DisasterManagement(db.Document):
    """ write model for  Disaster management  """

    query_class = QueryWrapper
    vehicle_id = db.StringField()
    accident_type = db.StringField()
    is_action_taken = db.IntField()
    description_of_action = db.StringField()
    is_repaired = db.IntField()
    date_of_accident = db.StringField()
    place_of_accident = db.StringField()
    is_critical = db.IntField()
    inspected_By = db.StringField()


class AnomalyOnBoard(db.Document):
    """   write model for Anomaly on board   """

    query_class = QueryWrapper
    anomaly_code = db.IntField()
    anomaly_message = db.StringField()
    anomaly_type = db.StringField()
    anomaly_part = db.StringField()
    diagnosis_function_name = db.StringField()
    timestamp = db.StringField()


class ErrorMapping(db.Document):
    """   write model for Anomaly on board   """

    query_class = QueryWrapper
    error_code = db.IntField()
    error_message = db.StringField()
    error_type = db.StringField()






