from flask import Flask
from flask_mongoalchemy import MongoAlchemy

app = Flask(__name__)
app.config['MONGOALCHEMY_DATABASE'] = 'abdummy'
app.config['MONOALCHEMY_CONNECTION_STRING'] = 'mongodb://localhost:27017/ab_dummy'

db = MongoAlchemy(app)
