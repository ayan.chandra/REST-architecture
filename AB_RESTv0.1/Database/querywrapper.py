from app.models import *
from flask_mongoalchemy import BaseQuery
from Database.dbconnect import *



class QueryWrapper(BaseQuery):
    """
        QueryWrapper body
    """
    def get_activity_data(self, vehicle_id):
        return self.filter(self.type.vehicle_id == vehicle_id)

    def get_vehicle_id(self, app_id):
        return self.filter(self.type.app_id == app_id)

    def check_vehicle_id(self, vehicle_id):
        return self.filter(self.type.vehicle_id == vehicle_id)

    # vehicle activity through vehicle id


def getVehicleActivity(vehicle_id):
    return VehicleActivity.query.get_activity_data(vehicle_id).first()


def checkVehicleId(vehicle_id):
    if VehicleManagement.query.check_vehicle_id(vehicle_id).first():
        return True
    else:
        return False

def getVehicleId(app_id):
    return VehicleAppMapping.query.get_vehicle_id(app_id).first()

# insert for VehicleActivity
def insert_vehicle_activity(data_object):
    vehicle_activity = VehicleActivity(vehicle_id=data_object['vehicle_id'],
                                           vehicle_activity_time=data_object['vehicle_activity_time'],
                                           vehicle_activity_status=data_object['vehicle_activity_status'])
    vehicle_activity.save()


def getRecordsFromVehicleManagement(vehicle_id):
    return VehicleManagement.query_class.get_vehicle_data(QueryWrapper,vehicle_id)

# insert for UserDetails
def insert_user_details(data_object):
    user_details = UserDetails(user_name=data_object['user_name'],
                                      password=data_object['password'],
                                      user_contact_number=data_object['user_contact_number'],
                                      user_mail_id=data_object['user_mail_id'],
                                      user_registration_id=data_object['user_registration_id'],
                                      user_license_id=data_object['user_license_id'],
                                      user_adhaar_id=data_object['user_adhaar_id'],
                                      user_id=data_object['user_id'],
                                      vehicle_list=data_object['vehicle_list'])
    user_details.save()


""" AppDetails """
# insert for AppDetails
def insert_app_details(data_object):
    app_details = AppDetails(app_id=data_object['app_id'],
                                     app_ver=data_object['app_ver'],
                                     current_allowed_min_ver=data_object['current_allowed_min_ver'])
    app_details.save()


""" VehicleUserMapping """
#insert for VehicleUserMapping
def insert_vu_map(data_object):
    vu_vehicle_map = VehicleUserMapping(vehicle_id=data_object['vehicle_id'],
                                        user_id=data_object['user_id'],
                                        current_rest_ver=data_object['current_rest_ver'],
                                        current_OTA_ver=data_object['current_OTA_ver'],
                                        purchase_Date=data_object['purchase_Date'],
                                        puchase_type=data_object['puchase_type'],
                                        termination_Date=data_object['termination_Date'])
    vu_vehicle_map.save()


""" VehicleAppMapping """
#insert for VehicleAppMapping
def insert_vehicle_app_map(data_object):
    v_app_map = VehicleAppMapping(vehicle_id=data_object['vehicle_id'],
                                  app_id=data_object['app_id'],
                                  positive_contactor_state=data_object['positive_contactor_state'],
                                  platform=data_object['platform'],
                                  auth_verified_on=data_object['auth_verified_on'])
    v_app_map.save()


""" VehicleSpecs """
def insert_vehicle_specs(data_object):
    vehicle_specs = VehicleSpecs(vehicle_id=data_object['vehicle_id'],
                                 vehicle_type=data_object['vehicle_type'],
                                 engine_type=data_object['engine_type'],
                                 engine_model=data_object['engine_model'],
                                 power_consumption=data_object['power_consumption'],
                                 motor_type=data_object['motor_type'],
                                 motor_model=data_object['motor_model'],
                                 acceleration=data_object['acceleration'],
                                 braking_power=data_object['braking_power'],
                                 brake_model=data_object['brake_model'],
                                 battery_type=data_object['battery_type'],
                                 battery_model=data_object['battery_model'])
    vehicle_specs.save()

""" VehicleTravelDetails """
def insert_vehicle_travel_details(data_object):
    vehicle_travel_details = VehicleTravelDetails(vehicle_id=data_object['vehicle_id'],
                                                  distance_covered=data_object['distance_covered'],
                                                  power_consumed=data_object['power_consumed'],
                                                  regenerative_braking_power=data_object['regenerative_braking_power'],
                                                  timestamp=data_object['timestamp'])
    vehicle_travel_details.save()

""" VehicleActivity """
# insert for VehicleActivity
def insert_vehicle_activity(data_object):
    vehicle_activity = VehicleActivity(vehicle_id=data_object['vehicle_id'],
                                       vehicle_activity_time=data_object['vehicle_activity_time'],
                                       vehicle_activity_status=data_object['vehicle_activity_status'])
    vehicle_activity.save()

""" VehicleIssuerManagement """
# insert for VehicleIssuerManagement
def insert_vehicle_issuer_management(data_object):
    vehicle_issuer_management = VehicleIssuerManagement(issuer_id=data_object['issuer_id'],
                                                        issuer_name=data_object['issuer_name'],
                                                        issuer_mail_id=data_object['issuer_mail_id'],
                                                        issuer_employee_code=data_object['issuer_employee_code'])
    vehicle_issuer_management.save()

